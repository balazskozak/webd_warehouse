Egyszerű, CLI alapú raktárkezelő demo alkalmazás.
Próbáltam az egyszerűségre törekedni, ez időnként az optimalizált és elegáns megoldások rovására ment.
A specifikáció nem tért ki ilyesmire, így hirtelen ötlettől vezérelve parancssorban futtatható, nincs frontend, nincs backend, illetve a raktárkezeléshez új osztályt iktattam 
be, amely az összes raktárépület "fölött" áll, egyfajta menedzser szerepben.
Négy darab tesztesetet készítettem a feladat szerint:

Test1.php  -  Felveszi a termékeket az egyik raktárba, és kiíratja a raktár tartalmát.
  
Test2.php  - Felveszi (felvenné) a termékeket, de a kiválasztott raktárban nincs elég hely, ezért hibaüzenettel leáll.

Test3.php  - Több terméket szeretne a raktárba felvenni, de nincs elég hely, ezért megpróbálja több raktárba elosztva eltárolni a termékeket, és sikerrel jár, ezt ki is írja, mit hová tárolt.

Test4.php  - Több terméket szeretne kivenni a raktárakból, de nincs olyan raktár, amiben mindegyik meglenne, ezért összeválogatja őket a raktárakból, és megjeleníti mi hol található.
