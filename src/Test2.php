<?php
/**
 * Test2.php
 * Demonstrálja: raktárépületek felvitelét, felvesz több terméket, de nincs elég hely nekik
 * 
 * usage php Test2.php
*/


//autoloader start
require __DIR__.'../../vendor/autoload.php';

//minden adatdeklaráció a config.php-ben
require __DIR__.'/config.php';


//--------------raktárak hozzáadása

// raktar broker init
$wbroker=new Balazs\WebdWarehouse\Classes\WarehouseBroker();

// raktár kapacitást levesszük a teszt erejéig
$warehouse1->setCapacity(1);
$warehouse2->setCapacity(1);

//raktárakat hozzáadjuk
$wbroker->addWarehouse($warehouse1);
$wbroker->addWarehouse($warehouse2);
//-------------raktárak kész



//-------------termékek listázása
println("Termékek listája");
hr();

//termekek listazasa
foreach ($products as $p) {
    $p->showProduct()->display();
}

println(count($products)." db termék elhelyezése a broker alapján.");
hr();

//a broker szerint melyik raktárépületben van a legtöbb hely? - ebben a tesztesetben nem ellenorzunk arra, hogy van-e elég hely
$preferred_warehouse=$wbroker->getPreferredWarehouse();
//ha elore akarjuk tudni elfer-e barhol, akkor paraméterként mehet a termékek száma
//$preferred_warehouse=$wbroker->getPreferredWarehouse(count($products));

println($preferred_warehouse->getName().' a kiválasztott raktárépület. Tárhely: '.$preferred_warehouse->getCurrentCapacity());
hr();

try {
    foreach ($products as $product) {
        $preferred_warehouse->addProduct($product);
    }
} catch (\Balazs\WebdWarehouse\Classes\Exceptions\NoStorageException $e) {
    println("Hiba! ".$product->getName()." termék már nem fér el a raktárban!");
}

println("Kész, enterrel visszatérhetsz a parancssorba.");
readline();

