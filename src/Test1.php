<?php
/**
 * Test1.php
 * Demonstrálja: raktárépületek felvitelét, felvesz több terméket, és kiíratja a raktár tartalmát
 * 
 * usage php Test1.php
*/


//ignite autoloader
require __DIR__.'../../vendor/autoload.php';

//minden adatdeklaráció a config.php-ben
require __DIR__.'/config.php';


//-------------raktárak hozzáadása
println("Raktárépületek felvitele");
hr();

// broker inicializálás
$wbroker=new Balazs\WebdWarehouse\Classes\WarehouseBroker();
// raktárak -> broker
$wbroker->addWarehouse($warehouse1);
$wbroker->addWarehouse($warehouse2);

println($warehouse1->getName().' raktárépület létrehozva.');
println($warehouse2->getName().' raktárépület létrehozva.');

println("Raktárépületek hozzáadva a brokerhez.");
hr();
//-------------raktárak kész



//-------------terméklista
println("Termékek listája");
hr();

//termekek listazasa
foreach ($products as $p) {
    $p->showProduct()->display();
}
//-------------termékek kész

println(count($products)." db termék elhelyezése a broker alapján.");
hr();

try {
    $preferred_warehouse=$wbroker->getPreferredWarehouse();
    
    println($preferred_warehouse->getName().' a kiválasztott raktárépület. Tárhely: '.$preferred_warehouse->getCurrentCapacity());
    hr();

    try {
        foreach ($products as $product) {
            $preferred_warehouse->addProduct($product);
        }
    } catch (\Balazs\WebdWarehouse\Classes\Exceptions\NoStorageException $e) {
        println($product->getName()." már nem fér el!");
    }

    println("A raktár tartalma:");
    $preferred_warehouse->showStorage()->display();
    
} catch (\Balazs\WebdWarehouse\Classes\Exceptions\NoCapacityException $e ) {
    //nincs eleg hely a raktarban - ebben az esetben ez az ág irreleváns
    println("Egy raktárépületbe nem fér be az összes termék.");
}

println("Kész, enterrel visszatérhetsz a parancssorba.");
readline();

