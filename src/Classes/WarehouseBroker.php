<?php

namespace Balazs\WebdWarehouse\Classes;

final class WarehouseBroker 
{
    private $_warehouses;
    
    /**
     * adds a warehouse to the wh pool
     * @param \Balazs\WebdWarehouse\Classes\Warehouse $wh
     */
    public function addWarehouse(Warehouse $wh) 
    {
        $this->_warehouses[$wh->getIdentifier()]=$wh;
    }
    
    /**
     * removes a warehouse from the pool
     * @param \Balazs\WebdWarehouse\Classes\Warehouse $wh
     */
    public function removeWarehouse(Warehouse $wh) 
    {
        unset($this->_warehouses[$wh->getIdentifier()]);
    }
    
    /**
     * 
     * @param int $id - warehouse identifier number
     * @return \Balazs\WebdWarehouse\Classes\Warehouse - the selected warehouse
     * @throws Exception - warehouse not found
     */
    public function getWarehouse($id) 
    {
        if (empty($this->_warehouses[$id])) throw new Exception('Warehouse not in the pool.');
        return $this->_warehouses[$id];
    }
    
    /**
     * returns array of warehouse ids where Product_sku can be found
     * @param string $product_sku
     * @return array
     */
    public function findProduct($product_sku)
    {
        $products=[];
        foreach ($this->_warehouses as $wh) {
            //retrieves product count based on warehouse id
            $has_product=$wh->hasProduct($product_sku);
            //only above zero quantity for making the array smaller and more readable
            if ($has_product) $products[$wh->getIdentifier()]=$has_product;
        }
        return $products;
    }
    
    /**
     * simply retrieve products if the warehouse has
     * @param array $products - product sku array
     */
    public function retrieveProducts($products)
    {
        $retrieved_products=[];
        $found=[];
        $not_found=[];
        
        //no quantity management, so we're getting on by simple, unique values
        $search=  array_unique($products);
        
        while (!empty($search)) {
            $product=array_pop($search);
            foreach ($this->_warehouses as $wh) {
                try {
                    $retrieved_products[$wh->getIdentifier()][]=$wh->retrieveProduct($product);
                    $found[$product]=$product;
                } catch (\Balazs\WebdWarehouse\Classes\Exceptions\ProductNotFoundException $e) {
                    $not_found[$product]=$product;
                    continue;
                } 
            }
        }
        
        //return errors too
        return [
            'found'=>$retrieved_products, 
            //this hurts a bit, sorry
            'errors'=>array_diff_key($not_found, $found),
            ];
    }
        
    /**
     * returns the preferred warehouse based on current free space
     * @return \Balazs\WebdWarehouse\Classes\Warehouse
     */
    public function getPreferredWarehouse($minimum_space=false)
    {
        //if there is a minimal space requirement, lets choose based on that
        if ($minimum_space) {
            $preferred=false;
            foreach ($this->_warehouses as $id=>$wh) {
                if ($wh->getCurrentCapacity()>$minimum_space) return $this->_warehouses[$id];
            }
            //none found, throw exception
            throw new \Balazs\WebdWarehouse\Classes\Exceptions\NoCapacityException;
        } else {
            //no minimum space parameter given, so schoose one based on current space
            
            $space=false;
            $preferred=false;
            foreach ($this->_warehouses as $id=>$wh) {
                //check for the greatest space
                if ($wh->getCurrentCapacity()>$space) {
                    $space=$wh->getCurrentCapacity();
                    $preferred=$id;
                }
            }
            
            if ($preferred) {
                //found a suitable warehouse
                return $this->_warehouses[$preferred];
            } else {
                throw new Exceptions\NoCapacityException;
            }
            
        }
    }
    
    /**
     * 
     * @param type $products
     * @return mixed array of warehouse ids
     */
    public function distributeProducts($products=[])
    {
        $space_required=count($products);
        
        $space_available=array_sum(array_map(function($wh) {
            return $wh->getCurrentCapacity();
        }, $this->_warehouses));
        
        if ($space_required>$space_available) throw new Exceptions\NoCapacityException;
        
        //seems we got enough space, but in different warehouses -> lets distribute the products
        $distribute=$products;
        $distributed_into=[];
        while (!empty($distribute)) {
            //getPreferredWarehouse will always find a suitable so no exception handling really required at this point
            $p=$this->getPreferredWarehouse();
            $p->addProduct(array_pop($distribute));
            if (empty($distributed_into[$p->getIdentifier()])) {
                //if not distributed into this wh before, make the counter 1
                $distributed_into[$p->getIdentifier()]=1;
            } else {
                //just increase the counter
                $distributed_into[$p->getIdentifier()]++;
            }
        }
        return $distributed_into;
    }
}