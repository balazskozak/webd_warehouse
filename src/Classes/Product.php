<?php

namespace Balazs\WebdWarehouse\Classes;

use LucidFrame\Console;

class Product 
{
    use \Balazs\WebdWarehouse\Traits\Dumpable;
    
    protected $_sku;
    protected $_name;
    protected $_price;
    protected $_brand;


    /**
     * returns the product unique sku id
     * @return string
     */
    public function getSku() {
        return $this->_sku;
    }
    
    /**
     * return product name
     * @return string
     */
    public function getName() 
    {
        return $this->_name." [".$this->_sku."]";
    }
    
    
    public function showProduct() 
    {
        $table=new \LucidFrame\Console\ConsoleTable();
        $table->addHeader('Kulcs')
                ->addHeader('Érték');
        foreach ($this as $key=>$value) {
            $table->addRow([$key, $value]);
        }
        return $table;
        
    }
    
    /**
     * 
     * @param type $args - array to set private keys
     */
    public function __construct($args) 
    {
        foreach($args as $key => $val) {
            $name = '_' . $key;
            $this->{$name} = $val;
        }     
    }
    
}