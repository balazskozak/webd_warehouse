<?php

namespace Balazs\WebdWarehouse\Classes\Exceptions;

class ProductNotFoundException extends \Exception {
    
    private $_product;
    
    public function __construct($product) {
        $this->product=$product;
        parent::__construct("Product not found!");
    }
    
    public function __toString() {
        return __CLASS__." says: ".$this->product." - Product not found!'";
    }
    
}
