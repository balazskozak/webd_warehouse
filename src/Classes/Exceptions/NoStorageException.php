<?php

namespace Balazs\WebdWarehouse\Classes\Exceptions;

class NoStorageException extends \Exception {
    

    public function __toString() {
        return __CLASS__." says: 'Not enough storage in warehouse! Do something!'";
    }
    
}
