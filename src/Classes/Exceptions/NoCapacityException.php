<?php

namespace Balazs\WebdWarehouse\Classes\Exceptions;

class NoCapacityException extends \Exception {
    
    public function __toString() {
        return __CLASS__." says: 'Not a single warehouse found that can handle the supplied quantity! Try to divide between warehouses.'";
    }
    
}
