<?php

namespace Balazs\WebdWarehouse\Classes;

use LucidFrame\Console;

class Warehouse 
{
    use \Balazs\WebdWarehouse\Traits\Dumpable;
    
    protected $_identifier;
    protected $_name;
    protected $_address;
    protected $_capacity;
    protected $_storage=[];
    
    
    /**
     * returns the identifier of the warehouse
     * @return int
     */
    public function getIdentifier()
    {
        return $this->_identifier;
    }
    
    /**
     * returns warehouse name
     * @return string
     */
    public function getName() 
    {
        return $this->_name;
    }
    
    /**
     * returns the current!! capacity
     * @return int
     */
    public function getCurrentCapacity() 
    {
        return ($this->_capacity - count($this->_storage));
    }
    
    
    /**
     * returns the overall capacity of the wh
     * @return int
     */
    public function getCapacity() 
    {
        return $this->_capacity;
    }
    
    /**
     * overrides the warehouses capacity - useful for the tests
     * @param int $capacity 
     */
    public function setCapacity($capacity) {
        $this->_capacity=($capacity<0) ? 0 : (int)$capacity;
    }
    
    /**
     * adds a product to the storage - based on simple hashing to avoid collision/overwrite
     * @param \Balazs\WebdWarehouse\Classes\Product $product
     */
    public function addProduct(\Balazs\WebdWarehouse\Classes\Product $product) 
    {
        if ($this->getCurrentStorage()<$this->_capacity) {
            $hash=spl_object_hash($product);
            $this->_storage[$hash]=$product;
        } else {
            throw new Exceptions\NoStorageException();
        }
    }
    
    /**
     * checks wether warehouse has a given product
     * @param string $product_sku
     * @return boolean
     */
    public function hasProduct($product_sku) 
    {
        $found=0;
        foreach ($this->_storage as $hash=>$item) {
            if ($item->getSku()==$product_sku) $found++;
        }
        return $found;
    }
    
    /**
     * retrieves a product from the warehouse
     * @param string $product_sku
     */
    public function retrieveProduct($product_sku)
    {
        //find product - this could be way better optimized bocs
        $found=false;
        foreach ($this->_storage as $hash=>$item) {
            if ($item->getSku()==$product_sku) {
                $found=$hash;
            }
        }
        if ($found) {
            $item=$this->_storage[$found];
            unset($this->_storage[$found]);
            return $item;
        }
        throw new Exceptions\ProductNotFoundException($product_sku);
    }
    
    /**
     * returns a CLI table with the current storage array
     */
    public function showStorage() 
    {
        $table=new \LucidFrame\Console\ConsoleTable();
        $table->addHeader('Hash')
                ->addHeader('Termék');
        foreach ($this->_storage as $hash=>$item) {
            $table->addRow([$hash, $item->getName()]);
        }
        $table->addBorderLine()->addRow(['Összesen:', count($this->_storage)]);
        return $table;
    }
    
    /**
     * returns the current storage count
     * @return int
     */
    public function getCurrentStorage() 
    {
        return count($this->_storage);
    }
    
    /**
     * 
     * @param type $args - lazy initilaziation for setting keys
     */
    public function __construct($args) 
    {
        foreach($args as $key => $val) {
            $name = '_' . $key;
            $this->{$name} = $val;
        }
    }    
}