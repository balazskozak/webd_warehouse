<?php

namespace Balazs\WebdWarehouse\Classes;

class Brand 
{
    use \Balazs\WebdWarehouse\Traits\Dumpable;
    
    protected $identifier;
    protected $name;
    protected $quality;


    /**
     * returns the name only - needed for the CLI table
     * @return string
     */
    public function __toString() 
    {
        return $this->name;
    }

    /**
     * 
     * @param int $id - brand unique id
     * @param string $name - brand name
     * @param int $quality - brand quality in (1..5) scale
     */
    
    public function __construct($id, $name, $quality) 
    {
        $this->identifier=$id;
        $this->name=$name;
        $this->quality=$quality;
    }
    
}