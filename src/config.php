<?php

/** the place for the dummy data */

$warehouse1=new \Balazs\WebdWarehouse\Classes\Warehouse([
    'identifier'=>1,
    'name'=>'Central raktár',
    'address'=>'1111 Budapest Bartók B. út 1',
    'capacity'=>'5',
    'storage'=>[],
]);


$warehouse2=new \Balazs\WebdWarehouse\Classes\Warehouse([
    'identifier'=>2,
    'name'=>'Szarvasi raktár',
    'address'=>'5540 Szarvas Szabadság út 30',
    'capacity'=>'4',
    'storage'=>[],
]);

$brands=[
    new Balazs\WebdWarehouse\Classes\Brand(1, 'Nike', 4),
    new Balazs\WebdWarehouse\Classes\Brand(2, 'Adidas', 5),
    new Balazs\WebdWarehouse\Classes\Brand(3, 'Adios', 1),
    new Balazs\WebdWarehouse\Classes\Brand(4, 'Head', 4),
];


$products=[
    new \Balazs\WebdWarehouse\Classes\Product([
        'sku'=>'NHB0002',
        'name'=>'Nike HeadBand Tourniquet 2',
        'price'=>'11320',
        'brand'=>$brands[0],
    ]),
    new Balazs\WebdWarehouse\Classes\Products\Trouser([
        'sku'=>'TTR0001',
        'name'=>'Rafa Nadal Slim trouser',
        'price'=>'8000',
        'brand'=>$brands[3],
        'size'=>'XL',
    ]),    
    new Balazs\WebdWarehouse\Classes\Products\Headband([
        'sku'=>'NHB0003',
        'name'=>'Seamless headband Head',
        'price'=>'9800',
        'brand'=>$brands[1],
        'color'=>'black',
    ]),
];
