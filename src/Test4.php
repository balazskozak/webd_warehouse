<?php
/**
 * Test4.php
 * Demonstrálja: raktárépületek felvitelét, felvesz több terméket, elosztja a termékekeket a raktárak között, majd kikéri őket - egy raktár nem tudja kiszolgálni, 
 * így több raktárból szedi a termékeket
 * 
 * usage php Test4.php
*/


//ignite autoloader
require __DIR__.'../../vendor/autoload.php';

//minden adatdeklaráció a config.php-ben
require __DIR__.'/config.php';


//-------------raktárak felvitele
println("Raktárépületek felvitele");
hr();

// raktárépület broker class
$wbroker=new Balazs\WebdWarehouse\Classes\WarehouseBroker();

// kapacitás csökkentése
$warehouse1->setCapacity(2);
$warehouse2->setCapacity(2);

// raktárak a brokerbe
$wbroker->addWarehouse($warehouse1);
$wbroker->addWarehouse($warehouse2);
//-------------raktárak kész



//-------------terméklista
println("Termékek listája");
hr();

//termekek listazasa
foreach ($products as $p) {
    $p->showProduct()->display();
}

println(count($products)." db termék elhelyezése a broker alapján.");
hr();

try {
    //szetszorja a termekeket, hogy tobb raktarbol kelljen kikerni
    // a $distributedben pedig megkapjuk mit hová rakott, hogy valamit ki is tudjunk írni a képernyőre
    $distributed=$wbroker->distributeProducts($products);
    foreach ($distributed as $whid=>$count) {
        println($wbroker->getWarehouse($whid)->getName()." befogadott ".$count." terméket.");
        $wbroker->getWarehouse($whid)->showStorage()->display();
    }
    
    hr();

    //nincs keszletkezeles, így a duplan beírt SKU nem kerül külön értelmezésre
    $products_to_retrieve=['NHB0002', 'TTR0001', 'NHB0003', 'DummySKU', 'NHB0003'];
    
    println("A következő termékeket keresem a raktárakban: ".implode(", ", $products_to_retrieve));
    hr();
    
    $retrieved=$wbroker->retrieveProducts($products_to_retrieve);
    if (!empty($retrieved['found'])) {
        //volt amit talalt
        foreach ($retrieved['found'] as $whid=>$products) {
            foreach ($products as $product) {
                println($product->getName()." terméket találtam a ".$wbroker->getWarehouse($whid)->getName()." raktárban.");
            }
        }
        //amit nem talalt, arrol is mondjunk valamit
        foreach ($retrieved['errors'] as $error) {
            println($error." terméket NEM találtam sehol. ");
        }
    } else {
        println("Egyetlen terméket sem találtam.");
    }

} catch (\Balazs\WebdWarehouse\Classes\Exceptions\NoStorageException $e) {
    //ha esetleg nem tudná elosztani - ebben a tesztben ez irreleváns
    println("A termékek elosztása nem lehetséges, nincs elegendő tárolóhely.");
}

println("Kész, enterrel visszatérhetsz a parancssorba.");
readline();

