<?php
/**
 * Test3.php
 * Demonstrálja: raktárépületek felvitelét, felvesz több terméket, de nincs elég hely nekik, a raktár-broker elosztja a termékekeket
 * 
 * usage php Test3.php
*/


//autoloader
require __DIR__.'../../vendor/autoload.php';

//minden adatdeklaráció a config.php-ben
require __DIR__.'/config.php';


//-------------raktárak
//
// raktár broker bekapcs
$wbroker=new Balazs\WebdWarehouse\Classes\WarehouseBroker();

//kapacitás csökkentés a teszt erejéig
$warehouse1->setCapacity(2);
$warehouse2->setCapacity(2);

//raktár hozzáadás
$wbroker->addWarehouse($warehouse1);
$wbroker->addWarehouse($warehouse2);

//-------------raktárak fent



//-------------termékek

//termekek listazasa
foreach ($products as $p) {
    $p->showProduct()->display();
}


println(count($products)." db termék elhelyezése a broker alapján.");
hr();

//termekek elhelyezése
try {
    //paraméterként a termékek száma, így egyből megtudjuk, hogy van-e elég hely, ha nincs, jön a catch ág - és most nem lesz elég hely
    $preferred_warehouse=$wbroker->getPreferredWarehouse(count($products));
    
    println($preferred_warehouse->getName().' a kiválasztott raktárépület. Tárhely: '.$preferred_warehouse->getCurrentCapacity());
    hr();
    
    //ha lenne elég hely, a termékek itt tárolódnának - a preferred warehouse egyértelművé teszi, hogy van hely a termékeknek, felesleges még egy try blokk
    foreach ($products as $product) {
        $preferred_warehouse->addProduct($product);
    }

    println("A raktár tartalma:");
    $preferred_warehouse->showStorage()->display();
    
} catch (\Balazs\WebdWarehouse\Classes\Exceptions\NoCapacityException $e ) {
    //nincs eleg hely egyik raktarban sem
    println("Egy raktárépületbe nem fér be az összes termék, így elosztom őket.");
    try {
        $distributed=$wbroker->distributeProducts($products);
        foreach ($distributed as $whid=>$count) {
            println($wbroker->getWarehouse($whid)->getName()." befogadott ".$count." terméket.");
            $wbroker->getWarehouse($whid)->showStorage()->display();
        }
    } catch (\Balazs\WebdWarehouse\Classes\Exceptions\NoStorageException $e) {
        //nem tudja elosztani sem
        println("A termékek elosztása nem lehetséges, nincs elegendő tárolóhely.");
    }
}

println("Kész, enterrel visszatérhetsz a parancssorba.");
readline();

