<?php

namespace Balazs\WebdWarehouse\Traits;

trait Dumpable {
    
    /**
     * shows the object properties in a less distracive way
     * @return string 
     */
    
    public function showProperties() {
        ob_start();
        echo '<pre>';
        print_r($this);
        echo '</pre>';
        $dump=ob_get_contents();
        ob_end_clean();
        return $dump;
    }
    
    /**
     * dumps any given variable in a pretty print
     * @param string $what
     * @return string
     */
    public function dump($what) {
        ob_start();
        echo '<pre>';
        var_dump($what);
        echo '</pre>';
        $dump=ob_get_contents();
        ob_end_clean();
        return $dump;
    }
    
    
}


