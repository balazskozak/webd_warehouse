<?php
/* room for some helpers if needed for the CLI */


if (!function_exists("hr")) {
    //a html hr :)
    function hr() {
        echo '____________________________________________'.PHP_EOL;
    }
   
}

if (!function_exists("println")) {
    //print line and EOL
    function println($what) {
        echo $what.PHP_EOL;
    }
   
}